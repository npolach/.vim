  


<!DOCTYPE html>
<html>
  <head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# githubog: http://ogp.me/ns/fb/githubog#">
    <meta charset='utf-8'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>vim-wombat256mod/colors/wombat256mod.vim at master · michalbachowski/vim-wombat256mod · GitHub</title>
    <link rel="search" type="application/opensearchdescription+xml" href="/opensearch.xml" title="GitHub" />
    <link rel="fluid-icon" href="https://github.com/fluidicon.png" title="GitHub" />
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="apple-touch-icon-114.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="apple-touch-icon-114.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="apple-touch-icon-144.png" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="apple-touch-icon-144.png" />
    <link rel="logo" type="image/svg" href="http://github-media-downloads.s3.amazonaws.com/github-logo.svg" />
    <meta name="msapplication-TileImage" content="/windows-tile.png">
    <meta name="msapplication-TileColor" content="#ffffff">

    
    
    <link rel="icon" type="image/x-icon" href="/favicon.ico" />

    <meta content="authenticity_token" name="csrf-param" />
<meta content="Y65saUAUyk2IIK3elClClQ5Sei61oZ8O9xa2VYG5gIg=" name="csrf-token" />

    <link href="https://a248.e.akamai.net/assets.github.com/assets/github-7bc5a6f32e2aa01d928235256cb8f50935425ec5.css" media="screen" rel="stylesheet" type="text/css" />
    <link href="https://a248.e.akamai.net/assets.github.com/assets/github2-4076c70526d6a8cd6ece6cf527916c222c323fa6.css" media="screen" rel="stylesheet" type="text/css" />
    


      <script src="https://a248.e.akamai.net/assets.github.com/assets/frameworks-5dcdaf734c8092261f37e6534c8f114696d913a9.js" type="text/javascript"></script>
      <script src="https://a248.e.akamai.net/assets.github.com/assets/github-47a3a09fdb39a3cc73da291f394b4c33a2695b7b.js" type="text/javascript"></script>
      

        <link rel='permalink' href='/michalbachowski/vim-wombat256mod/blob/76158d25a7fedda20cecf24530f8118da6586b02/colors/wombat256mod.vim'>
    <meta property="og:title" content="vim-wombat256mod"/>
    <meta property="og:type" content="githubog:gitrepository"/>
    <meta property="og:url" content="https://github.com/michalbachowski/vim-wombat256mod"/>
    <meta property="og:image" content="https://secure.gravatar.com/avatar/dd4dbf0f55889ad804872255679c6865?s=420&amp;d=https://a248.e.akamai.net/assets.github.com%2Fimages%2Fgravatars%2Fgravatar-user-420.png"/>
    <meta property="og:site_name" content="GitHub"/>
    <meta property="og:description" content="vim-wombat256mod - Wombat256 vim theme packaged to work with Tim Pope's pathogen plugin"/>
    <meta property="twitter:card" content="summary"/>
    <meta property="twitter:site" content="@GitHub">
    <meta property="twitter:title" content="michalbachowski/vim-wombat256mod"/>

    <meta name="description" content="vim-wombat256mod - Wombat256 vim theme packaged to work with Tim Pope's pathogen plugin" />

  <link href="https://github.com/michalbachowski/vim-wombat256mod/commits/master.atom" rel="alternate" title="Recent Commits to vim-wombat256mod:master" type="application/atom+xml" />

  </head>


  <body class="logged_out page-blob  vis-public env-production  ">
    <div id="wrapper">

      

      

      

      


        <div class="header header-logged-out">
          <div class="container clearfix">

            <a class="header-logo-wordmark" href="https://github.com/">
              <img alt="GitHub" class="github-logo-4x" height="30" src="https://a248.e.akamai.net/assets.github.com/images/modules/header/logov7@4x.png?1338945075" />
              <img alt="GitHub" class="github-logo-4x-hover" height="30" src="https://a248.e.akamai.net/assets.github.com/images/modules/header/logov7@4x-hover.png?1338945075" />
            </a>

              
<ul class="top-nav">
    <li class="explore"><a href="https://github.com/explore">Explore GitHub</a></li>
  <li class="search"><a href="https://github.com/search">Search</a></li>
  <li class="features"><a href="https://github.com/features">Features</a></li>
    <li class="blog"><a href="https://github.com/blog">Blog</a></li>
</ul>


            <div class="header-actions">
                <a class="button primary" href="https://github.com/signup">Sign up for free</a>
              <a class="button" href="https://github.com/login?return_to=%2Fmichalbachowski%2Fvim-wombat256mod%2Fblob%2Fmaster%2Fcolors%2Fwombat256mod.vim">Sign in</a>
            </div>

          </div>
        </div>


      

      


            <div class="site hfeed" itemscope itemtype="http://schema.org/WebPage">
      <div class="hentry">
        
        <div class="pagehead repohead instapaper_ignore readability-menu">
          <div class="container">
            <div class="title-actions-bar">
              


<ul class="pagehead-actions">



    <li>
      <a href="/login?return_to=%2Fmichalbachowski%2Fvim-wombat256mod"
        class="minibutton js-toggler-target star-button entice tooltipped upwards"
        title="You must be signed in to use this feature" rel="nofollow">
        <span class="mini-icon mini-icon-star"></span>Star
      </a>
      <a class="social-count js-social-count" href="/michalbachowski/vim-wombat256mod/stargazers">
        4
      </a>
    </li>
    <li>
      <a href="/login?return_to=%2Fmichalbachowski%2Fvim-wombat256mod"
        class="minibutton js-toggler-target fork-button entice tooltipped upwards"
        title="You must be signed in to fork a repository" rel="nofollow">
        <span class="mini-icon mini-icon-fork"></span>Fork
      </a>
      <a href="/michalbachowski/vim-wombat256mod/network" class="social-count">
        3
      </a>
    </li>
</ul>

              <h1 itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="entry-title public">
                <span class="repo-label"><span>public</span></span>
                <span class="mega-icon mega-icon-public-repo"></span>
                <span class="author vcard">
                  <a href="/michalbachowski" class="url fn" itemprop="url" rel="author">
                  <span itemprop="title">michalbachowski</span>
                  </a></span> /
                <strong><a href="/michalbachowski/vim-wombat256mod" class="js-current-repository">vim-wombat256mod</a></strong>
              </h1>
            </div>

            

  <ul class="tabs">
    <li><a href="/michalbachowski/vim-wombat256mod" class="selected" highlight="repo_sourcerepo_downloadsrepo_commitsrepo_tagsrepo_branches">Code</a></li>
    <li><a href="/michalbachowski/vim-wombat256mod/network" highlight="repo_network">Network</a></li>
    <li><a href="/michalbachowski/vim-wombat256mod/pulls" highlight="repo_pulls">Pull Requests <span class='counter'>0</span></a></li>

      <li><a href="/michalbachowski/vim-wombat256mod/issues" highlight="repo_issues">Issues <span class='counter'>0</span></a></li>



    <li><a href="/michalbachowski/vim-wombat256mod/graphs" highlight="repo_graphsrepo_contributors">Graphs</a></li>


  </ul>
  
<div class="tabnav">

  <span class="tabnav-right">
    <ul class="tabnav-tabs">
          <li><a href="/michalbachowski/vim-wombat256mod/tags" class="tabnav-tab" highlight="repo_tags">Tags <span class="counter blank">0</span></a></li>
    </ul>
    
  </span>

  <div class="tabnav-widget scope">


    <div class="select-menu js-menu-container js-select-menu js-branch-menu">
      <a class="minibutton select-menu-button js-menu-target" data-hotkey="w" data-ref="master">
        <span class="mini-icon mini-icon-branch"></span>
        <i>branch:</i>
        <span class="js-select-button">master</span>
      </a>

      <div class="select-menu-modal-holder js-menu-content js-navigation-container js-select-menu-pane">

        <div class="select-menu-modal js-select-menu-pane">
          <div class="select-menu-header">
            <span class="select-menu-title">Switch branches/tags</span>
            <span class="mini-icon mini-icon-remove-close js-menu-close"></span>
          </div> <!-- /.select-menu-header -->

          <div class="select-menu-filters">
            <div class="select-menu-text-filter">
              <input type="text" id="commitish-filter-field" class="js-select-menu-text-filter js-filterable-field js-navigation-enable" placeholder="Filter branches/tags">
            </div> <!-- /.select-menu-text-filter -->
            <div class="select-menu-tabs">
              <ul>
                <li class="select-menu-tab">
                  <a href="#" data-tab-filter="branches" class="js-select-menu-tab">Branches</a>
                </li>
                <li class="select-menu-tab">
                  <a href="#" data-tab-filter="tags" class="js-select-menu-tab">Tags</a>
                </li>
              </ul>
            </div><!-- /.select-menu-tabs -->
          </div><!-- /.select-menu-filters -->

          <div class="select-menu-list select-menu-tab-bucket js-select-menu-tab-bucket css-truncate" data-tab-filter="branches" data-filterable-for="commitish-filter-field" data-filterable-type="substring">



              <div class="select-menu-item js-navigation-item js-navigation-target selected">
                <span class="select-menu-checkmark mini-icon mini-icon-confirm"></span>
                <a href="/michalbachowski/vim-wombat256mod/blob/master/colors/wombat256mod.vim" class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target" data-name="master" rel="nofollow" title="master">master</a>
              </div> <!-- /.select-menu-item -->

              <div class="select-menu-no-results js-not-filterable">Nothing to show</div>
          </div> <!-- /.select-menu-list -->


          <div class="select-menu-list select-menu-tab-bucket js-select-menu-tab-bucket css-truncate" data-tab-filter="tags" data-filterable-for="commitish-filter-field" data-filterable-type="substring">


            <div class="select-menu-no-results js-not-filterable">Nothing to show</div>

          </div> <!-- /.select-menu-list -->

        </div> <!-- /.select-menu-modal -->
      </div> <!-- /.select-menu-modal-holder -->
    </div> <!-- /.select-menu -->

  </div> <!-- /.scope -->

  <ul class="tabnav-tabs">
    <li><a href="/michalbachowski/vim-wombat256mod" class="selected tabnav-tab" highlight="repo_source">Files</a></li>
    <li><a href="/michalbachowski/vim-wombat256mod/commits/master" class="tabnav-tab" highlight="repo_commits">Commits</a></li>
    <li><a href="/michalbachowski/vim-wombat256mod/branches" class="tabnav-tab" highlight="repo_branches" rel="nofollow">Branches <span class="counter ">1</span></a></li>
  </ul>

</div>

  
  
  


            
          </div>
        </div><!-- /.repohead -->

        <div id="js-repo-pjax-container" class="container context-loader-container" data-pjax-container>
          


<!-- blob contrib key: blob_contributors:v21:381691f0e69882fe8c4c42fd9efe5dcf -->
<!-- blob contrib frag key: views10/v8/blob_contributors:v21:381691f0e69882fe8c4c42fd9efe5dcf -->


<div id="slider">
    <div class="frame-meta">

      <p title="This is a placeholder element" class="js-history-link-replace hidden"></p>

        <div class="breadcrumb">
          <span class='bold'><span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/michalbachowski/vim-wombat256mod" class="js-slide-to" data-direction="back" itemscope="url"><span itemprop="title">vim-wombat256mod</span></a></span></span> / <span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/michalbachowski/vim-wombat256mod/tree/master/colors" class="js-slide-to" data-direction="back" itemscope="url"><span itemprop="title">colors</span></a></span> / <strong class="final-path">wombat256mod.vim</strong> <span class="js-zeroclipboard zeroclipboard-button" data-clipboard-text="colors/wombat256mod.vim" data-copied-hint="copied!" title="copy to clipboard"><span class="mini-icon mini-icon-clipboard"></span></span>
        </div>

      <a href="/michalbachowski/vim-wombat256mod/find/master" class="js-slide-to" data-hotkey="t" style="display:none">Show File Finder</a>


        
  <div class="commit file-history-tease">
    <img class="main-avatar" height="24" src="https://secure.gravatar.com/avatar/dd4dbf0f55889ad804872255679c6865?s=140&amp;d=https://a248.e.akamai.net/assets.github.com%2Fimages%2Fgravatars%2Fgravatar-user-420.png" width="24" />
    <span class="author"><a href="/michalbachowski" rel="author">michalbachowski</a></span>
    <time class="js-relative-date" datetime="2011-09-30T04:40:14-07:00" title="2011-09-30 04:40:14">September 30, 2011</time>
    <div class="commit-title">
        <a href="/michalbachowski/vim-wombat256mod/commit/76158d25a7fedda20cecf24530f8118da6586b02" class="message">Initial import</a>
    </div>

    <div class="participation">
      <p class="quickstat"><a href="#blob_contributors_box" rel="facebox"><strong>1</strong> contributor</a></p>
      
    </div>
    <div id="blob_contributors_box" style="display:none">
      <h2>Users on GitHub who have contributed to this file</h2>
      <ul class="facebox-user-list">
        <li>
          <img height="24" src="https://secure.gravatar.com/avatar/dd4dbf0f55889ad804872255679c6865?s=140&amp;d=https://a248.e.akamai.net/assets.github.com%2Fimages%2Fgravatars%2Fgravatar-user-420.png" width="24" />
          <a href="/michalbachowski">michalbachowski</a>
        </li>
      </ul>
    </div>
  </div>


    </div><!-- ./.frame-meta -->

    <div class="frames">
      <div class="frame" data-permalink-url="/michalbachowski/vim-wombat256mod/blob/76158d25a7fedda20cecf24530f8118da6586b02/colors/wombat256mod.vim" data-title="vim-wombat256mod/colors/wombat256mod.vim at master · michalbachowski/vim-wombat256mod · GitHub" data-type="blob">

        <div id="files" class="bubble">
          <div class="file">
            <div class="meta">
              <div class="info">
                <span class="icon"><b class="mini-icon mini-icon-text-file"></b></span>
                <span class="mode" title="File Mode">file</span>
                  <span>97 lines (83 sloc)</span>
                <span>3.372 kb</span>
              </div>
              <div class="actions">
                <div class="button-group">
                      <a class="minibutton js-entice" href=""
                         data-entice="You must be signed in and on a branch to make or propose changes">Edit</a>
                  <a href="/michalbachowski/vim-wombat256mod/raw/master/colors/wombat256mod.vim" class="button minibutton " id="raw-url">Raw</a>
                    <a href="/michalbachowski/vim-wombat256mod/blame/master/colors/wombat256mod.vim" class="button minibutton ">Blame</a>
                  <a href="/michalbachowski/vim-wombat256mod/commits/master/colors/wombat256mod.vim" class="button minibutton " rel="nofollow">History</a>
                </div><!-- /.button-group -->
              </div><!-- /.actions -->

            </div>
                <div class="data type-viml js-blob-data">
      <table cellpadding="0" cellspacing="0" class="lines">
        <tr>
          <td>
            <pre class="line_numbers"><span id="L1" rel="#L1">1</span>
<span id="L2" rel="#L2">2</span>
<span id="L3" rel="#L3">3</span>
<span id="L4" rel="#L4">4</span>
<span id="L5" rel="#L5">5</span>
<span id="L6" rel="#L6">6</span>
<span id="L7" rel="#L7">7</span>
<span id="L8" rel="#L8">8</span>
<span id="L9" rel="#L9">9</span>
<span id="L10" rel="#L10">10</span>
<span id="L11" rel="#L11">11</span>
<span id="L12" rel="#L12">12</span>
<span id="L13" rel="#L13">13</span>
<span id="L14" rel="#L14">14</span>
<span id="L15" rel="#L15">15</span>
<span id="L16" rel="#L16">16</span>
<span id="L17" rel="#L17">17</span>
<span id="L18" rel="#L18">18</span>
<span id="L19" rel="#L19">19</span>
<span id="L20" rel="#L20">20</span>
<span id="L21" rel="#L21">21</span>
<span id="L22" rel="#L22">22</span>
<span id="L23" rel="#L23">23</span>
<span id="L24" rel="#L24">24</span>
<span id="L25" rel="#L25">25</span>
<span id="L26" rel="#L26">26</span>
<span id="L27" rel="#L27">27</span>
<span id="L28" rel="#L28">28</span>
<span id="L29" rel="#L29">29</span>
<span id="L30" rel="#L30">30</span>
<span id="L31" rel="#L31">31</span>
<span id="L32" rel="#L32">32</span>
<span id="L33" rel="#L33">33</span>
<span id="L34" rel="#L34">34</span>
<span id="L35" rel="#L35">35</span>
<span id="L36" rel="#L36">36</span>
<span id="L37" rel="#L37">37</span>
<span id="L38" rel="#L38">38</span>
<span id="L39" rel="#L39">39</span>
<span id="L40" rel="#L40">40</span>
<span id="L41" rel="#L41">41</span>
<span id="L42" rel="#L42">42</span>
<span id="L43" rel="#L43">43</span>
<span id="L44" rel="#L44">44</span>
<span id="L45" rel="#L45">45</span>
<span id="L46" rel="#L46">46</span>
<span id="L47" rel="#L47">47</span>
<span id="L48" rel="#L48">48</span>
<span id="L49" rel="#L49">49</span>
<span id="L50" rel="#L50">50</span>
<span id="L51" rel="#L51">51</span>
<span id="L52" rel="#L52">52</span>
<span id="L53" rel="#L53">53</span>
<span id="L54" rel="#L54">54</span>
<span id="L55" rel="#L55">55</span>
<span id="L56" rel="#L56">56</span>
<span id="L57" rel="#L57">57</span>
<span id="L58" rel="#L58">58</span>
<span id="L59" rel="#L59">59</span>
<span id="L60" rel="#L60">60</span>
<span id="L61" rel="#L61">61</span>
<span id="L62" rel="#L62">62</span>
<span id="L63" rel="#L63">63</span>
<span id="L64" rel="#L64">64</span>
<span id="L65" rel="#L65">65</span>
<span id="L66" rel="#L66">66</span>
<span id="L67" rel="#L67">67</span>
<span id="L68" rel="#L68">68</span>
<span id="L69" rel="#L69">69</span>
<span id="L70" rel="#L70">70</span>
<span id="L71" rel="#L71">71</span>
<span id="L72" rel="#L72">72</span>
<span id="L73" rel="#L73">73</span>
<span id="L74" rel="#L74">74</span>
<span id="L75" rel="#L75">75</span>
<span id="L76" rel="#L76">76</span>
<span id="L77" rel="#L77">77</span>
<span id="L78" rel="#L78">78</span>
<span id="L79" rel="#L79">79</span>
<span id="L80" rel="#L80">80</span>
<span id="L81" rel="#L81">81</span>
<span id="L82" rel="#L82">82</span>
<span id="L83" rel="#L83">83</span>
<span id="L84" rel="#L84">84</span>
<span id="L85" rel="#L85">85</span>
<span id="L86" rel="#L86">86</span>
<span id="L87" rel="#L87">87</span>
<span id="L88" rel="#L88">88</span>
<span id="L89" rel="#L89">89</span>
<span id="L90" rel="#L90">90</span>
<span id="L91" rel="#L91">91</span>
<span id="L92" rel="#L92">92</span>
<span id="L93" rel="#L93">93</span>
<span id="L94" rel="#L94">94</span>
<span id="L95" rel="#L95">95</span>
<span id="L96" rel="#L96">96</span>
</pre>
          </td>
          <td width="100%">
                  <div class="highlight"><pre><div class='line' id='LC1'><span class="c">&quot; Vim color file</span></div><div class='line' id='LC2'><span class="c">&quot; Original Maintainer:  Lars H. Nielsen (dengmao@gmail.com)</span></div><div class='line' id='LC3'><span class="c">&quot; Last Change:  2010-07-23</span></div><div class='line' id='LC4'><span class="c">&quot;</span></div><div class='line' id='LC5'><span class="c">&quot; Modified version of wombat for 256-color terminals by</span></div><div class='line' id='LC6'><span class="c">&quot;   David Liang (bmdavll@gmail.com)</span></div><div class='line' id='LC7'><span class="c">&quot; based on version by</span></div><div class='line' id='LC8'><span class="c">&quot;   Danila Bespalov (danila.bespalov@gmail.com)</span></div><div class='line' id='LC9'><br/></div><div class='line' id='LC10'><span class="k">set</span> <span class="nb">background</span><span class="p">=</span><span class="nb">dark</span></div><div class='line' id='LC11'><br/></div><div class='line' id='LC12'><span class="k">if</span> <span class="k">version</span> <span class="p">&gt;</span> <span class="m">580</span></div><div class='line' id='LC13'>	<span class="k">hi</span> clear</div><div class='line' id='LC14'>	<span class="k">if</span> exists<span class="p">(</span><span class="s2">&quot;syntax_on&quot;</span><span class="p">)</span></div><div class='line' id='LC15'>		<span class="nb">syntax</span> reset</div><div class='line' id='LC16'>	<span class="k">endif</span></div><div class='line' id='LC17'><span class="k">endif</span></div><div class='line' id='LC18'><br/></div><div class='line' id='LC19'><span class="k">let</span> colors_name <span class="p">=</span> <span class="s2">&quot;wombat256mod&quot;</span></div><div class='line' id='LC20'><br/></div><div class='line' id='LC21'><br/></div><div class='line' id='LC22'><span class="c">&quot; General colors</span></div><div class='line' id='LC23'><span class="k">hi</span> Normal		ctermfg<span class="p">=</span><span class="m">252</span>		ctermbg<span class="p">=</span><span class="m">234</span>		cterm<span class="p">=</span>none		guifg<span class="p">=</span><span class="mh">#e3e0d7</span>	guibg<span class="p">=</span><span class="mh">#242424</span>	<span class="k">gui</span><span class="p">=</span>none</div><div class='line' id='LC24'><span class="k">hi</span> Cursor		ctermfg<span class="p">=</span><span class="m">234</span>		ctermbg<span class="p">=</span><span class="m">228</span>		cterm<span class="p">=</span>none		guifg<span class="p">=</span><span class="mh">#242424</span>	guibg<span class="p">=</span><span class="mh">#eae788</span>	<span class="k">gui</span><span class="p">=</span>none</div><div class='line' id='LC25'><span class="k">hi</span> Visual		ctermfg<span class="p">=</span><span class="m">251</span>		ctermbg<span class="p">=</span><span class="m">239</span>		cterm<span class="p">=</span>none		guifg<span class="p">=</span><span class="mh">#c3c6ca</span>	guibg<span class="p">=</span><span class="mh">#554d4b</span>	<span class="k">gui</span><span class="p">=</span>none</div><div class='line' id='LC26'><span class="k">hi</span> VisualNOS	ctermfg<span class="p">=</span><span class="m">251</span>		ctermbg<span class="p">=</span><span class="m">236</span>		cterm<span class="p">=</span>none		guifg<span class="p">=</span><span class="mh">#c3c6ca</span>	guibg<span class="p">=</span><span class="mh">#303030</span>	<span class="k">gui</span><span class="p">=</span>none</div><div class='line' id='LC27'><span class="k">hi</span> Search		ctermfg<span class="p">=</span><span class="m">177</span>		ctermbg<span class="p">=</span><span class="m">241</span>		cterm<span class="p">=</span>none		guifg<span class="p">=</span><span class="mh">#d787ff</span>	guibg<span class="p">=</span><span class="mh">#636066</span>	<span class="k">gui</span><span class="p">=</span>none</div><div class='line' id='LC28'><span class="k">hi</span> Folded		ctermfg<span class="p">=</span><span class="m">103</span>		ctermbg<span class="p">=</span><span class="m">237</span>		cterm<span class="p">=</span>none		guifg<span class="p">=</span><span class="mh">#a0a8b0</span>	guibg<span class="p">=</span><span class="mh">#3a4046</span>	<span class="k">gui</span><span class="p">=</span>none</div><div class='line' id='LC29'><span class="k">hi</span> Title		ctermfg<span class="p">=</span><span class="m">230</span>						cterm<span class="p">=</span><span class="nb">bold</span>		guifg<span class="p">=</span><span class="mh">#ffffd7</span>					<span class="k">gui</span><span class="p">=</span><span class="nb">bold</span></div><div class='line' id='LC30'><span class="k">hi</span> StatusLine	ctermfg<span class="p">=</span><span class="m">230</span>		ctermbg<span class="p">=</span><span class="m">238</span>		cterm<span class="p">=</span>none		guifg<span class="p">=</span><span class="mh">#ffffd7</span>	guibg<span class="p">=</span><span class="mh">#444444</span>	<span class="k">gui</span><span class="p">=</span><span class="nb">italic</span></div><div class='line' id='LC31'><span class="k">hi</span> VertSplit	ctermfg<span class="p">=</span><span class="m">238</span>		ctermbg<span class="p">=</span><span class="m">238</span>		cterm<span class="p">=</span>none		guifg<span class="p">=</span><span class="mh">#444444</span>	guibg<span class="p">=</span><span class="mh">#444444</span>	<span class="k">gui</span><span class="p">=</span>none</div><div class='line' id='LC32'><span class="k">hi</span> StatusLineNC	ctermfg<span class="p">=</span><span class="m">241</span>		ctermbg<span class="p">=</span><span class="m">238</span>		cterm<span class="p">=</span>none		guifg<span class="p">=</span><span class="mh">#857b6f</span>	guibg<span class="p">=</span><span class="mh">#444444</span>	<span class="k">gui</span><span class="p">=</span>none</div><div class='line' id='LC33'><span class="k">hi</span> LineNr		ctermfg<span class="p">=</span><span class="m">241</span>		ctermbg<span class="p">=</span><span class="m">232</span>		cterm<span class="p">=</span>none		guifg<span class="p">=</span><span class="mh">#857b6f</span>	guibg<span class="p">=</span><span class="mh">#080808</span>	<span class="k">gui</span><span class="p">=</span>none</div><div class='line' id='LC34'><span class="k">hi</span> SpecialKey	ctermfg<span class="p">=</span><span class="m">241</span>		ctermbg<span class="p">=</span><span class="m">235</span>		cterm<span class="p">=</span>none		guifg<span class="p">=</span><span class="mh">#626262</span>	guibg<span class="p">=</span><span class="mh">#2b2b2b</span>	<span class="k">gui</span><span class="p">=</span>none</div><div class='line' id='LC35'><span class="k">hi</span> WarningMsg	ctermfg<span class="p">=</span><span class="m">203</span>										guifg<span class="p">=</span><span class="mh">#ff5f55</span></div><div class='line' id='LC36'><span class="k">hi</span> ErrorMsg		ctermfg<span class="p">=</span><span class="m">196</span>		ctermbg<span class="p">=</span><span class="m">236</span>		cterm<span class="p">=</span><span class="nb">bold</span>		guifg<span class="p">=</span><span class="mh">#ff2026</span>	guibg<span class="p">=</span><span class="mh">#3a3a3a</span>	<span class="k">gui</span><span class="p">=</span><span class="nb">bold</span></div><div class='line' id='LC37'><br/></div><div class='line' id='LC38'><span class="c">&quot; Vim &gt;= 7.0 specific colors</span></div><div class='line' id='LC39'><span class="k">if</span> <span class="k">version</span> <span class="p">&gt;=</span> <span class="m">700</span></div><div class='line' id='LC40'><span class="k">hi</span> CursorLine					ctermbg<span class="p">=</span><span class="m">236</span>		cterm<span class="p">=</span>none						guibg<span class="p">=</span><span class="mh">#32322f</span></div><div class='line' id='LC41'><span class="k">hi</span> MatchParen	ctermfg<span class="p">=</span><span class="m">228</span>		ctermbg<span class="p">=</span><span class="m">101</span>		cterm<span class="p">=</span><span class="nb">bold</span>		guifg<span class="p">=</span><span class="mh">#eae788</span>	guibg<span class="p">=</span><span class="mh">#857b6f</span>	<span class="k">gui</span><span class="p">=</span><span class="nb">bold</span></div><div class='line' id='LC42'><span class="k">hi</span> Pmenu		ctermfg<span class="p">=</span><span class="m">230</span>		ctermbg<span class="p">=</span><span class="m">238</span>						guifg<span class="p">=</span><span class="mh">#ffffd7</span>	guibg<span class="p">=</span><span class="mh">#444444</span></div><div class='line' id='LC43'><span class="k">hi</span> PmenuSel		ctermfg<span class="p">=</span><span class="m">232</span>		ctermbg<span class="p">=</span><span class="m">192</span>						guifg<span class="p">=</span><span class="mh">#080808</span>	guibg<span class="p">=</span><span class="mh">#cae982</span></div><div class='line' id='LC44'><span class="k">endif</span></div><div class='line' id='LC45'><br/></div><div class='line' id='LC46'><span class="c">&quot; Diff highlighting</span></div><div class='line' id='LC47'><span class="k">hi</span> DiffAdd						ctermbg<span class="p">=</span><span class="m">17</span>										guibg<span class="p">=</span><span class="mh">#2a0d6a</span></div><div class='line' id='LC48'><span class="k">hi</span> DiffDelete	ctermfg<span class="p">=</span><span class="m">234</span>		ctermbg<span class="p">=</span><span class="m">60</span>		cterm<span class="p">=</span>none		guifg<span class="p">=</span><span class="mh">#242424</span>	guibg<span class="p">=</span><span class="mh">#3e3969</span>	<span class="k">gui</span><span class="p">=</span>none</div><div class='line' id='LC49'><span class="k">hi</span> DiffText						ctermbg<span class="p">=</span><span class="m">53</span>		cterm<span class="p">=</span>none						guibg<span class="p">=</span><span class="mh">#73186e</span>	<span class="k">gui</span><span class="p">=</span>none</div><div class='line' id='LC50'><span class="k">hi</span> DiffChange					ctermbg<span class="p">=</span><span class="m">237</span>										guibg<span class="p">=</span><span class="mh">#382a37</span></div><div class='line' id='LC51'><br/></div><div class='line' id='LC52'><span class="c">&quot;hi CursorIM</span></div><div class='line' id='LC53'><span class="c">&quot;hi Directory</span></div><div class='line' id='LC54'><span class="c">&quot;hi IncSearch</span></div><div class='line' id='LC55'><span class="c">&quot;hi Menu</span></div><div class='line' id='LC56'><span class="c">&quot;hi ModeMsg</span></div><div class='line' id='LC57'><span class="c">&quot;hi MoreMsg</span></div><div class='line' id='LC58'><span class="c">&quot;hi PmenuSbar</span></div><div class='line' id='LC59'><span class="c">&quot;hi PmenuThumb</span></div><div class='line' id='LC60'><span class="c">&quot;hi Question</span></div><div class='line' id='LC61'><span class="c">&quot;hi Scrollbar</span></div><div class='line' id='LC62'><span class="c">&quot;hi SignColumn</span></div><div class='line' id='LC63'><span class="c">&quot;hi SpellBad</span></div><div class='line' id='LC64'><span class="c">&quot;hi SpellCap</span></div><div class='line' id='LC65'><span class="c">&quot;hi SpellLocal</span></div><div class='line' id='LC66'><span class="c">&quot;hi SpellRare</span></div><div class='line' id='LC67'><span class="c">&quot;hi TabLine</span></div><div class='line' id='LC68'><span class="c">&quot;hi TabLineFill</span></div><div class='line' id='LC69'><span class="c">&quot;hi TabLineSel</span></div><div class='line' id='LC70'><span class="c">&quot;hi Tooltip</span></div><div class='line' id='LC71'><span class="c">&quot;hi User1</span></div><div class='line' id='LC72'><span class="c">&quot;hi User9</span></div><div class='line' id='LC73'><span class="c">&quot;hi WildMenu</span></div><div class='line' id='LC74'><br/></div><div class='line' id='LC75'><br/></div><div class='line' id='LC76'><span class="c">&quot; Syntax highlighting</span></div><div class='line' id='LC77'><span class="k">hi</span> Keyword		ctermfg<span class="p">=</span><span class="m">111</span>		cterm<span class="p">=</span>none		guifg<span class="p">=</span><span class="mh">#88b8f6</span>	<span class="k">gui</span><span class="p">=</span>none</div><div class='line' id='LC78'><span class="k">hi</span> Statement	ctermfg<span class="p">=</span><span class="m">111</span>		cterm<span class="p">=</span>none		guifg<span class="p">=</span><span class="mh">#88b8f6</span>	<span class="k">gui</span><span class="p">=</span>none</div><div class='line' id='LC79'><span class="k">hi</span> Constant		ctermfg<span class="p">=</span><span class="m">173</span>		cterm<span class="p">=</span>none		guifg<span class="p">=</span><span class="mh">#e5786d</span>	<span class="k">gui</span><span class="p">=</span>none</div><div class='line' id='LC80'><span class="k">hi</span> Number		ctermfg<span class="p">=</span><span class="m">173</span>		cterm<span class="p">=</span>none		guifg<span class="p">=</span><span class="mh">#e5786d</span>	<span class="k">gui</span><span class="p">=</span>none</div><div class='line' id='LC81'><span class="k">hi</span> PreProc		ctermfg<span class="p">=</span><span class="m">173</span>		cterm<span class="p">=</span>none		guifg<span class="p">=</span><span class="mh">#e5786d</span>	<span class="k">gui</span><span class="p">=</span>none</div><div class='line' id='LC82'><span class="k">hi</span> Function		ctermfg<span class="p">=</span><span class="m">192</span>		cterm<span class="p">=</span>none		guifg<span class="p">=</span><span class="mh">#cae982</span>	<span class="k">gui</span><span class="p">=</span>none</div><div class='line' id='LC83'><span class="k">hi</span> Identifier	ctermfg<span class="p">=</span><span class="m">192</span>		cterm<span class="p">=</span>none		guifg<span class="p">=</span><span class="mh">#cae982</span>	<span class="k">gui</span><span class="p">=</span>none</div><div class='line' id='LC84'><span class="k">hi</span> Type			ctermfg<span class="p">=</span><span class="m">186</span>		cterm<span class="p">=</span>none		guifg<span class="p">=</span><span class="mh">#d4d987</span>	<span class="k">gui</span><span class="p">=</span>none</div><div class='line' id='LC85'><span class="k">hi</span> Special		ctermfg<span class="p">=</span><span class="m">229</span>		cterm<span class="p">=</span>none		guifg<span class="p">=</span><span class="mh">#eadead</span>	<span class="k">gui</span><span class="p">=</span>none</div><div class='line' id='LC86'><span class="k">hi</span> String		ctermfg<span class="p">=</span><span class="m">113</span>		cterm<span class="p">=</span>none		guifg<span class="p">=</span><span class="mh">#95e454</span>	<span class="k">gui</span><span class="p">=</span><span class="nb">italic</span></div><div class='line' id='LC87'><span class="k">hi</span> Comment		ctermfg<span class="p">=</span><span class="m">246</span>		cterm<span class="p">=</span>none		guifg<span class="p">=</span><span class="mh">#9c998e</span>	<span class="k">gui</span><span class="p">=</span><span class="nb">italic</span></div><div class='line' id='LC88'><span class="k">hi</span> Todo			ctermfg<span class="p">=</span><span class="m">101</span>		cterm<span class="p">=</span>none		guifg<span class="p">=</span><span class="mh">#857b6f</span>	<span class="k">gui</span><span class="p">=</span><span class="nb">italic</span></div><div class='line' id='LC89'><br/></div><div class='line' id='LC90'><br/></div><div class='line' id='LC91'><span class="c">&quot; Links</span></div><div class='line' id='LC92'><span class="k">hi</span><span class="p">!</span> link FoldColumn		Folded</div><div class='line' id='LC93'><span class="k">hi</span><span class="p">!</span> link CursorColumn	CursorLine</div><div class='line' id='LC94'><span class="k">hi</span><span class="p">!</span> link NonText		LineNr</div><div class='line' id='LC95'><br/></div><div class='line' id='LC96'><span class="c">&quot; vim:set ts=4 sw=4 noet:</span></div></pre></div>
          </td>
        </tr>
      </table>
  </div>

          </div>
        </div>

        <a href="#jump-to-line" rel="facebox" data-hotkey="l" class="js-jump-to-line" style="display:none">Jump to Line</a>
        <div id="jump-to-line" style="display:none">
          <h2>Jump to Line</h2>
          <form accept-charset="UTF-8" class="js-jump-to-line-form">
            <input class="textfield js-jump-to-line-field" type="text">
            <div class="full-button">
              <button type="submit" class="button">Go</button>
            </div>
          </form>
        </div>

      </div>
    </div>
</div>

<div id="js-frame-loading-template" class="frame frame-loading large-loading-area" style="display:none;">
  <img class="js-frame-loading-spinner" src="https://a248.e.akamai.net/assets.github.com/images/spinners/octocat-spinner-128.gif?1347543527" height="64" width="64">
</div>


        </div>
      </div>
      <div class="context-overlay"></div>
    </div>

      <div id="footer-push"></div><!-- hack for sticky footer -->
    </div><!-- end of wrapper - hack for sticky footer -->

      <!-- footer -->
      <div id="footer">
  <div class="container clearfix">

      <dl class="footer_nav">
        <dt>GitHub</dt>
        <dd><a href="https://github.com/about">About us</a></dd>
        <dd><a href="https://github.com/blog">Blog</a></dd>
        <dd><a href="https://github.com/contact">Contact &amp; support</a></dd>
        <dd><a href="http://enterprise.github.com/">GitHub Enterprise</a></dd>
        <dd><a href="http://status.github.com/">Site status</a></dd>
      </dl>

      <dl class="footer_nav">
        <dt>Applications</dt>
        <dd><a href="http://mac.github.com/">GitHub for Mac</a></dd>
        <dd><a href="http://windows.github.com/">GitHub for Windows</a></dd>
        <dd><a href="http://eclipse.github.com/">GitHub for Eclipse</a></dd>
        <dd><a href="http://mobile.github.com/">GitHub mobile apps</a></dd>
      </dl>

      <dl class="footer_nav">
        <dt>Services</dt>
        <dd><a href="http://get.gaug.es/">Gauges: Web analytics</a></dd>
        <dd><a href="http://speakerdeck.com">Speaker Deck: Presentations</a></dd>
        <dd><a href="https://gist.github.com">Gist: Code snippets</a></dd>
        <dd><a href="http://jobs.github.com/">Job board</a></dd>
      </dl>

      <dl class="footer_nav">
        <dt>Documentation</dt>
        <dd><a href="http://help.github.com/">GitHub Help</a></dd>
        <dd><a href="http://developer.github.com/">Developer API</a></dd>
        <dd><a href="http://github.github.com/github-flavored-markdown/">GitHub Flavored Markdown</a></dd>
        <dd><a href="http://pages.github.com/">GitHub Pages</a></dd>
      </dl>

      <dl class="footer_nav">
        <dt>More</dt>
        <dd><a href="http://training.github.com/">Training</a></dd>
        <dd><a href="https://github.com/edu">Students &amp; teachers</a></dd>
        <dd><a href="http://shop.github.com">The Shop</a></dd>
        <dd><a href="/plans">Plans &amp; pricing</a></dd>
        <dd><a href="http://octodex.github.com/">The Octodex</a></dd>
      </dl>

      <hr class="footer-divider">


    <p class="right">&copy; 2013 <span title="0.04328s from fe2.rs.github.com">GitHub</span> Inc. All rights reserved.</p>
    <a class="left" href="https://github.com/">
      <span class="mega-icon mega-icon-invertocat"></span>
    </a>
    <ul id="legal">
        <li><a href="https://github.com/site/terms">Terms of Service</a></li>
        <li><a href="https://github.com/site/privacy">Privacy</a></li>
        <li><a href="https://github.com/security">Security</a></li>
    </ul>

  </div><!-- /.container -->

</div><!-- /.#footer -->


    <div class="fullscreen-overlay js-fullscreen-overlay" id="fullscreen_overlay">
  <div class="fullscreen-container js-fullscreen-container">
    <div class="textarea-wrap">
      <textarea name="fullscreen-contents" id="fullscreen-contents" class="js-fullscreen-contents" placeholder="" data-suggester="fullscreen_suggester"></textarea>
          <div class="suggester-container">
              <div class="suggester fullscreen-suggester js-navigation-container" id="fullscreen_suggester"
                 data-url="/michalbachowski/vim-wombat256mod/suggestions/commit/76158d25a7fedda20cecf24530f8118da6586b02">
              </div>
          </div>
    </div>
  </div>
  <div class="fullscreen-sidebar">
    <a href="#" class="exit-fullscreen js-exit-fullscreen tooltipped leftwards" title="Exit Zen Mode">
      <span class="mega-icon mega-icon-normalscreen"></span>
    </a>
    <a href="#" class="theme-switcher js-theme-switcher tooltipped leftwards"
      title="Switch themes">
      <span class="mini-icon mini-icon-brightness"></span>
    </a>
  </div>
</div>



    <div id="ajax-error-message" class="flash flash-error">
      <span class="mini-icon mini-icon-exclamation"></span>
      Something went wrong with that request. Please try again.
      <a href="#" class="mini-icon mini-icon-remove-close ajax-error-dismiss"></a>
    </div>

    
    
    <span id='server_response_time' data-time='0.04375' data-host='fe2'></span>
    
  </body>
</html>

