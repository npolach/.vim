" This line should not be removed as it ensures that various options are
" properly set to work with the Vim-related packages.
runtime! archlinux.vim

" Options ----------------------------------------------------------------------

" Automatic relading of .vimrc
autocmd! bufwritepost .vimrc source %

" Use syntax highlighting.
syntax on
set t_Co=256
set background=dark
"color jellybeans
"color wombat256mod

" Show whitespaces
highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/
autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
"autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
"autocmd InsertLeave * match ExtraWhitespace /\s\+$/
autocmd BufWinLeave * call clearmatches()

" width of document
" set tw=79
set colorcolumn=80
highlight ColorColumn ctermbg=233

" Show numbers
set number

" Enables Vim command autocompletion
set wildmenu
set wildmode=list:longest,full
set wildignore=*.o,*~,*.pyc,*.pyo,*.so,*.sw*,__pycache__

"Enables omnicompletion
filetype plugin on
set ofu=syntaxcomplete#Complete

" maps leader key to comma
let mapleader=","

" Disables auto commenting
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" Remember 1000 changes/commands.
set history=1000
set undolevels=1000

" Set tab to use 4 spaces instead of tab.
set softtabstop=4
set shiftwidth=4
set shiftround
set expandtab
set tabstop=4

" Set lines not wrap when to long to display
set nowrap
" Show line#/column#/location% in bottom
" right corner.
"set ruler

" Highlight search terms and dynamically
" search as term is being typed.
set hlsearch
set incsearch

" Ignore case in search term unless
" there is a capital letter.
set ignorecase
set smartcase

" Disables backups and swap files
"set nobackup
set nowritebackup
set noswapfile

" Better navigating through omnicomplete option list
set completeopt=longest,menuone
function! OmniPopup(action)
    if pumvisible()
        if a:action == 'j'
            return "\<C-N>"
        elseif a:action == 'k'
            return "\<C-P>"
        endif
    endif
    return a:action
endfunction

inoremap <silent><C-j> <C-R>=OmniPopup('j')<CR>
inoremap <silent><C-k> <C-R>=OmniPopup('k')<CR>

" Plugins ---------------------------------------------------------------------

" Pathogen
execute pathogen#infect()

" Powerline
set laststatus=2

" CTRLP
let g:ctrlp_map = '<C-o>'
let g:ctrlp_max_height=30
set wildignore+=*.pyc
set wildignore+=*_build/*
set wildignore+=*/converage/*

" Python folding
set nofoldenable

" Python Mode

" Load the whole plugin
"let g:pymode = 1

" Load pylint code plugin
" let g:pymode_lint = 1

" Disable pylint checking every save
"let g:pymode_lint_write = 0

" Set key 'R' for run python code
"let g:pymode_run_key = '<leader>r'

" Key Bindings ----------------------------------------------------------------

" Disable arrow keys
map <up> <nop>
map <down> <nop>
map <left> <nop>
map <right> <nop>
imap <up> <nop>
imap <down> <nop>
imap <left> <nop>
imap <right> <nop>

" Navigates through tabs
map <Leader>> :tabn<CR>
map <Leader>< :tabp<CR>

" Opens file in new tab
map + :tabe

" map sort function to a key
vnoremap <Leader>s :sort<CR>

" Toggles task list pane
map T :TaskList<CR>

" Toggles tag list pane
map P :TlistToggle<CR>

" Toggles NERDTree
map <C-n> :NERDTreeToggle<CR>
